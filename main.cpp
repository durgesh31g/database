#include <QCoreApplication>
//#include <QtSql/QSqlDatabase>
#include <QSqlDatabase>
#include <QtSql>
#include <QtDebug>
#include <QPluginLoader>
#include <QSqlDriver>
#include <QSqlQuery>

/*
//this connect is written for mysql
void Connect(){

    QSqlDatabase db = QSqlDatabase::addDatabase("QMYSQL3");
    db.setHostName("localhost");
    db.setUserName("root");
    db.setPassword("seat@2005");
    db.setDatabaseName("world");
    bool value = db.open();

    qDebug() <<db.lastError().text();
    qDebug()<<"Data base status: "<<value;
}
*/

//This connect is used for SQLServer2012
void Connect(){

    QString driver      = "SQL Server Native Client 11.0";
    QString servername  = "DS-8CG91637VJ";
    QString db_name     = "DU_Test";
    QString ur_name     = "durgesh_k";
    QString pwd         = "";


    QSqlDatabase db = QSqlDatabase::addDatabase("QODBC");
    qDebug()<<"Result:"<<db.isValid();
    qDebug()<<"Result:"<<db.isDriverAvailable("QODBC");

    db.setDatabaseName(QString("DRIVER={%1}; SERVER=%2;"
                               "DATABASE=%3;UID=%4;PWD=%5;"
                               "Trusted_connection=yes").arg(driver)
                       .arg(servername)
                       .arg(db_name)
                       .arg(ur_name)
                       .arg(pwd));

    bool value = db.open();
    qDebug() <<db.lastError().text();
    qDebug()<<"Data base status: "<<value;
}

bool testplugin(){
    qInfo()<<"Test pluging";
    QPluginLoader loader("C:/Qt/Qt5.13.2/5.13.2/mingw73_64/bin/qsqlmysqld.dll");
    qInfo()<<loader.metaData();
    if(loader.load()){
        qInfo()<<"Drive is loaded";
        return true;
    }

    qInfo()<<loader.errorString();
    return false;
}

void createTable(){
    QSqlQuery qry;
    qry.prepare("CREATE TABLE tb_Studen(id INT PRIMARY KEY,fristname VARCHAR(30),lastname VARCHAR(30))");
    if(!qry.exec()){
        qDebug()<<qry.lastError().text();

    }else{
        qDebug()<<"Table has been created";
    }
}

int main(int argc, char *argv[])
{
    QCoreApplication a(argc, argv);
    // if(testplugin()){
    Connect();
    // }
    createTable();


    return a.exec();
}
